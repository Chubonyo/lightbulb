=============
API Reference
=============

Command Handler
===============
.. automodule:: lightbulb.command_handler
    :members:
    :member-order: groupwise

----

Commands
========

.. automodule:: lightbulb.commands
    :members:
    :show-inheritance:

----

Plugins
=======

.. automodule:: lightbulb.plugins
    :members:

----

Checks
======

Note that all check decorators **must** be above the command decorator otherwise your code will not work.

.. automodule:: lightbulb.checks
    :members:

----

Context
=======

.. automodule:: lightbulb.context
    :members:
    :member-order: groupwise

----

Converters
==========

.. automodule:: lightbulb.converters
    :members:
    :show-inheritance:
    :member-order: bysource

----

Cooldowns
=========

.. automodule:: lightbulb.cooldowns
    :members:
    :show-inheritance:
    :member-order: bysource

----

Errors
======

.. automodule:: lightbulb.errors
    :members:
    :show-inheritance:
    :member-order: bysource

----

Help
====

.. automodule:: lightbulb.help
    :members:
